 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Form Data</title>
 </head>
 <body>
     <form action="" method="post">
         <input type="number" placeholder="Enter Number 1" name="number1">
         <input type="number" placeholder="Enter Number 2" name="number2  ">
         <button type="submit">Submit</button>
     </form>
 </body>
 </html>
 
 
 <?php
class Calculator
{
    public $number1;
    public $number2;

    public function __construct($num1, $num2)
    {
        $this->number1 = $num1;
        $this->number2 = $num2;
    }
    public function add()
    {
        return $this->number1 + $this->number2;
    }
    public function sub()
    {
        return $this->number1 - $this->number2;
    }
    public function mul()
    {
        return $this->number1 * $this->number2;
    }
    public function div()
    {
        return $this->number1 / $this->number2;
    }

}

$calculatorObject = new Calculator(20, 10);
echo "The Addition is : ". $calculatorObject->add();
echo "<br>";
echo "The Substruction is :". $calculatorObject->sub();
echo "<br>";
echo "The Multiplication is : ". $calculatorObject->mul();
echo "<br>";
echo "The Divission is :  ". $calculatorObject->div();