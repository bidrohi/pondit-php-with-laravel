<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login page</title>
</head>
<body>
    <?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message']; 
        unset($_SESSION['message']);
    }
       
    ?>
<form action="login_process.php" method ="POST">
        <input type="text" placeholder = "Enter Username" name="username">
        <input type="password" placeholder = "Enter Password" name="password">
        <button type="submit">Log In</button>

    </form>
</body>
</html>