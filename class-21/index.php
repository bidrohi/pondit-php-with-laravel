 <?php

// class Greeter 
// {
//     public $message = " Hello , I am Sarker Bidrohi ";
//     public function hello()
//     {
//             return $this->message;

//     }

// }

// $greeterObject = new Greeter();
// echo $greeterObject ->message;
// echo "<br>";
// echo $greeterObject->hello();

// q-4:
// class Car
// {
//     public $company = Null;
//     public $color = 'beige';

//     public function describe()
//     {
        
//         return "Beep! I am $this->company, my color is $this->color";
//     }
// }

// $bmwObject = new Car();
// $bmwObject->company= 'bmw';

// echo $bmwObject->describe();

// question-5;
// class Calculator 
// {
//     public $multiply_result;
//     public $add_result;
//     public $substraction_result;
//     public $division_result;

//     public function multiply($num1, $num2)
//     {
//         $this->multiply_result= $num1 * $num2;
//     }
//     public function add($num1, $num2)
//     {
//         $this->add_result= $num1 + $num2;
//     }
//     public function sub($num1, $num2)
//     {
//         $this->substraction_result= $num1 - $num2;
//     }
//     public function div($num1, $num2)
//     {
//         $this->division_result= $num1 / $num2;
//     }
// }

// $calculator = new Calculator();

// echo $calculator->multiply(22,23);
// echo $calculator->multiply_result;
// echo "<br>";
// echo $calculator->add(22,23);
// echo $calculator->add_result;
// echo "<br>";
// echo $calculator->sub(22,23);
// echo $calculator->substraction_result;
// echo "<br>";
// echo $calculator->div(22,23);
// echo $calculator->division_result;
// echo "<br>";


class Student 
{
    public $firstname;
    public $lastname;
    public $age;

    public function setProperties($fname, $lname, $age)
    {
        $this->firstname = $fname;
        $this->lastname = $lname;
        $this->age= $age;
    }
}

$student1= new Student();
$student1->setProperties('Jerry', 'way', 34);



$student2= new Student();
$student2->setProperties('Jhon', 'wilimas', 38);


$student3= new Student();
$student3->setProperties('sarker', 'Bidrohi', 22);


$student4= new Student();
$student4->setProperties('Sayem', 'Patoary', 25);


$student5= new Student();
$student5->setProperties('Resed', 'Ahamed', 30);
   

$sum = ($student1->age+$student2->age+$student3->age+$student4->age+$student5->age);
$avg= $sum/5;

echo $avg;

?>